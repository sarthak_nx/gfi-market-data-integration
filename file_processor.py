import properties as p
import json, os
from gateway.gfi.fx_option import FX_OPTION as gfi_fxo
from gateway.fmd.ir_data import FMD_IR_RATES as fmd_ir
from gateway.fmd.fx_data import FMD_FX_RATES as fmd_fx
from gateway.gfi.ir_option import IR_OPTION as gfi_iro
from gateway.gfi.ir_data import GFI_IR_RATES as gfi_ir
from gateway.rpm.ir_data import RPM_IR_RATES as rpm_ir
from gateway.cct.ir_data import CCT_IR_RATES as cct_ir
from gateway.gfi.ir_bonds import IR_BONDS as gfi_bonds
from gateway.gfi.cds import CDS_CURVE as gfi_cds
from gateway.gfi.cdx import CDS_INDEX as gfi_cdx

class FileProcessor:

    def __init__(self, ftp_object, request_path, sub_directory, file_name):

        self.ftp_object = ftp_object
        self.request_path = request_path
        self.sub_directory = sub_directory
        self.file_name = file_name

    def get_ftp_file_config(self):

        with open(os.path.join(p.CONFIG_PATH, p.FILE_TYPE_PROPERTIES), 'r') as fp:
            self.file_type_properties = json.load(fp)
            fp.close()

    def load_file_config(self):

        self.get_ftp_file_config()
        all_file_types = []
        for k, v in self.file_type_properties.items():
            all_file_types.extend(list(x for x in v))

        file_property = [x for x in all_file_types if x['path'] == self.request_path][0]

        return file_property

    def processor(self):

        file_property = self.load_file_config()

        class_to_call = file_property['processing_class']
        print('Processing', class_to_call)
        p.log.info('Processing ' + class_to_call)
        class_name = getattr(self, class_to_call, None)
        status, file_size = class_name()

        return status, file_size

    def GFI_IR_BONDS(self):

        status, file_size = gfi_bonds(self.ftp_object, self.request_path, self.sub_directory, self.file_name).process_request('IRBond', 'GFIIR', 'Intraday', 'Default')
        return status, file_size

    def GFI_FX_OPTION(self):

        status, file_size = gfi_fxo(self.ftp_object, self.request_path, self.sub_directory, self.file_name).process_request('FXOption', 'GFIFXO', 'Intraday', 'Default')
        return status, file_size

    def FMD_IR_RATES(self):

        status, file_size = fmd_ir(self.ftp_object, self.request_path, self.sub_directory, self.file_name).process_request('IRRate', 'FMDIR', 'Intraday', 'Default')
        return status, file_size

    def FMD_FX_RATES(self):

        status, file_size = fmd_fx(self.ftp_object, self.request_path, self.sub_directory, self.file_name).process_request('FXRate', 'FMDFX', 'Intraday', 'Default')
        return status, file_size

    def GFI_IR_OPTION(self):

        status, file_size = gfi_iro(self.ftp_object, self.request_path, self.sub_directory, self.file_name).process_request('IROption', 'GFIIRO', 'Intraday', 'Default')
        return status, file_size

    def GFI_IR_RATES(self):

        status, file_size = gfi_ir(self.ftp_object, self.request_path, self.sub_directory, self.file_name).process_request('IRRate', 'GFIIR', 'Intraday', 'Default')
        return status, file_size

    def RPM_IR_RATES(self):

        status, file_size = rpm_ir(self.ftp_object, self.request_path, self.sub_directory, self.file_name).process_request('IRRate', 'RPMIR', 'Intraday', 'Default')
        return status, file_size

    def CCT_IR_RATES(self):

        status, file_size = cct_ir(self.ftp_object, self.request_path, self.sub_directory, self.file_name).process_request('IRRate', 'CCTIR', 'Intraday', 'Default')
        return status, file_size

    def GFI_CR_CURVE(self):

        status, file_size = gfi_cds(self.ftp_object, self.request_path, self.sub_directory, self.file_name).process_request('CRCDS', 'GFICDS', 'Intraday', 'Default')
        return status, file_size

    def GFI_CR_INDEX(self):

        status, file_size = gfi_cdx(self.ftp_object, self.request_path, self.sub_directory, self.file_name).process_request('CRCDS', 'GFICDX', 'Intraday', 'Default')
        return status, file_size