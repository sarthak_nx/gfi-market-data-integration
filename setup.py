from setuptools import setup

setup(
    name='gfi-market-source-integration',
    version='0.1-SNAPSHOT',
    packages=['configs', 'gateway', 'gateway.cct', 'gateway.config', 'gateway.fmd', 'gateway.gfi', 'gateway.others', 'gateway.rpm', 'gateway.utils'],
    url='',
    license='',
    author='Sarthak Shreya',
    author_email='sshreya@numerix.com',
    description='Integration between GFI market data sources(via FTP) to market-data-service',
    install_requires = ['pytz', 'pandas>0.23.1', 'requests', 'openpyxl']
)
