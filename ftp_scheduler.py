import os, sys
from datetime import datetime
from pytz import timezone
from file_handler import FileHandler
from gateway import properties as ov_p
from gateway.utils.request_group_excel_to_json import ExcelToJson
import properties as p
import logging

def main():

    local_date = datetime.now()
    print('Triggered FTP Scheduler at', local_date)
    p.log.info('Triggered FTP Scheduler at ' + local_date.strftime('%Y-%m-%d'))
    eastern = timezone('US/Eastern')
    est_dt = datetime.now(eastern).date()


    print('Generating request config.')
    p.log.info('Generating request config.')
    xl = ExcelToJson()
    request_mapping = xl.excel_to_json(os.path.join(ov_p.MAPPING_CONFIG_PATH, ov_p.MAPPING_CONFIG_XL_NAME),
                                       os.path.join(ov_p.MAPPING_CONFIG_PATH, ov_p.MAPPING_CONFIG_JSON_NAME))

    print('Triggering data load.')
    p.log.info('Triggering data load.')
    try:
        est_dt = est_dt
        f = FileHandler(est_dt)
        f.get_ftp_file_listing()
    except Exception as error:
        print('Error occured. Exiting from ftp_scheduler.py')
        p.log.error('Error occured. Exiting from ftp_scheduler.py')
        p.log.exception(error)
        sys.exit()

main()