import properties as p
from gateway.rpm.data_processor import RPM_DATA

class RPM_IR_RATES(RPM_DATA):

    def __init__(self, ftp_object, path, sub_directory, file_name):
        p.log.debug('Initializing.')
        super().__init__(ftp_object, path, sub_directory, file_name)