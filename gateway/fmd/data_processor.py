import pandas as pd

import gzip, os
from io import StringIO
import properties as p
from gateway.base_data_processor import BaseDataProcesser
pd.options.mode.chained_assignment = None

class FMD_DATA(BaseDataProcesser):

    def __init__(self, ftp_object,  path = None, sub_directory = None,  file_name = None):

        super().__init__(ftp_object=ftp_object,path = path,file_name = file_name,sub_directory= sub_directory)

    def load_data(self):

        if self.sub_directory:
            self.ftp_object.cwd(self.request_url + self.sub_directory)
        else:
            self.ftp_object.cwd(self.request_url)

        data_bytes = bytearray()
        def handle_binary(more_data):
            data_bytes.extend(more_data)

        self.ftp_object.retrbinary("RETR " + self.file_name, callback=handle_binary)
        self.file_size = self.ftp_object.size(self.file_name)
        print('Size', "{0:.4f}".format(self.file_size / 1048576), 'mb')
        p.log.info('Size ' + "{0:.4f}".format(self.file_size / 1048576) + 'mb')
        byte_to_str = str(gzip.decompress(data_bytes), 'utf-8')

        data_orig = pd.read_csv(StringIO(byte_to_str))
        data = data_orig

        if data.empty == False:

            new_data = pd.DataFrame([], columns=self.columns)
            if self.columns == list(data.columns):
                data = data.iloc[1:]
            else:
                new_data = new_data.append(pd.Series(list(data.columns), index=self.columns), ignore_index=True)
            data.columns = self.columns
            new_data = new_data.append(data, ignore_index=True)

            return new_data

    def load_data_local(self):

        data_orig = pd.read_csv(os.path.join(self.request_url, self.file_name))

        data = data_orig

        if data.empty == False:

            new_data = pd.DataFrame([], columns=self.columns)
            if self.columns == list(data.columns):
                data = data.iloc[1:]
            else:
                new_data = new_data.append(pd.Series(list(data.columns), index=self.columns), ignore_index=True)
            data.columns = self.columns
            new_data = new_data.append(data, ignore_index=True)

            return new_data
        else:
            return pd.DataFrame([])