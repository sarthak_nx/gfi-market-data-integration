import properties as p
from gateway.fmd.data_processor import FMD_DATA

class FMD_IR_RATES(FMD_DATA):

    def __init__(self, ftp_object, path, sub_directory, file_name):
        p.log.debug('Initializing.')
        super().__init__(ftp_object, path, sub_directory, file_name)