from gateway.utils.ov_reference_utils import quote_upload
from gateway.base_data_processor import BaseDataProcesser
import properties as p

class GFI_DATA(BaseDataProcesser):

    def __init__(self, ftp_object,  path = None, sub_directory = None,  file_name = None):

        super().__init__(ftp_object=ftp_object,path = path,file_name = file_name,sub_directory= sub_directory)

