import pandas as pd, time
import properties as p
from gateway import properties as ov_p
from gateway.gfi.data_processor import GFI_DATA
from gateway.utils import ov_reference_utils as utils

class CDS_CURVE(GFI_DATA):

    def __init__(self, ftp_object, path, sub_directory, file_name):
        p.log.debug('Initializing.')
        super().__init__(ftp_object, path, sub_directory, file_name)

    def filter_data(self, data):

        data = self.create_date_time_fields(data)

        cleaning_time = time.time() - self.start_time
        upload_time = cleaning_time

        all_quotes = pd.DataFrame([])
        for inst in self.request_filters:
            filters = inst['values'][0]

            data_filters = [ov_p.OV_QUOTE_DATE_TIME_FIELD, ov_p.OV_QUOTE_DATE_FIELD]
            data_filters = data_filters + filters['identifierFields'] + [filters['bidQuoteField'],filters['askQuoteField']]
            if set(data_filters).issubset(data.columns):

                if filters['dataFilterField']:
                    filtered_data = data[data[filters['dataFilterField']].isin(filters['dataFilterValue'])]
                else:
                    filtered_data = data
                if filters['excDataFilterValue']:
                    filtered_data = filtered_data[filtered_data[filters['dataFilterField']].str.contains('|'.join(filters['excDataFilterValue'])) == False]
                filtered_data['baseDescription'] = filters['baseDescription']
                if filters['auxIdentifierFields']:
                    for x, y in zip(filters['auxIdentifierFields'], filters['auxIdentifierValue']):
                        filtered_data[x] = y

                    data_filters = data_filters + filters['auxIdentifierFields']
                else:
                    filters['auxIdentifierFields'] = []
                if filtered_data.empty == False:

                    filter_fields = [x for x in self.request_filter_fields if x['filterName'] == inst['filterName']][0]
                    sub_process_fields = list(
                        dict(field_name=x['fieldName'], sub_processing_method=x['subProcessingMethod']) for x in
                        filter_fields['values'] if x['subProcessingMethod'])
                    for pf in sub_process_fields:
                        try:
                            filtered_data[pf['field_name']] = filtered_data.apply(
                                lambda x: eval(pf['sub_processing_method']), axis=1)
                        except Exception as error:
                            p.log.exception(error)
                            continue

                    quotes = self.transform_data_to_quote_bean(filtered_data, filters['baseDescription'],
                                                               filters['idFields'],
                                                               filters['identifierFields'] + filters[
                                                                   'auxIdentifierFields'],
                                                               filters['bidQuoteField'], filters['askQuoteField'],
                                                               filters['quoteType'], filters['quoteScalingFactor'], filters['auxFields'])
                    processing_time = time.time() - upload_time
                    print(filters['filterName'])
                    p.log.info(filters['filterName'])
                    all_quotes = all_quotes.append(quotes, ignore_index=True)
                    upload_time = time.time() - processing_time
                else:
                    print('No data found for', filters['filterName'])
                    p.log.warning('No data found for ' + filters['filterName'])
                

        status = self.upload_quotes_to_ov('CDS', all_quotes)

        return status
