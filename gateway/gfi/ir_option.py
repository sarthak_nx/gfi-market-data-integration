import pandas as pd, time
import properties as p
from gateway import properties as ov_p
from gateway.gfi.data_processor import GFI_DATA


class IR_OPTION(GFI_DATA):

    def __init__(self, ftp_object, path, sub_directory, file_name):
        p.log.debug('Initializing.')
        super().__init__(ftp_object, path, sub_directory, file_name)

    def filter_data(self, data):

        status = None
        data = self.create_date_time_fields(data)

        cleaning_time = time.time() - self.start_time
        upload_time = cleaning_time
        for inst in self.request_filters:
            filters = inst['values'][0]

            data_filters = [ov_p.OV_QUOTE_DATE_TIME_FIELD, ov_p.OV_QUOTE_DATE_FIELD]
            data_filters = data_filters + filters['idFields'] + [filters['bidQuoteField'],filters['askQuoteField']]

            if set(data_filters).issubset(data.columns):

                if filters['dataFilterField'] and filters['dataFilterField'] == 'file_name':
                    if any(x in self.file_name for x in filters['dataFilterValue']):
                        filtered_data = data
                        if filters['auxIdentifierFields']:
                            for x, y in zip(filters['auxIdentifierFields'], filters['auxIdentifierValue']):
                                filtered_data[x] = y

                            data_filters = data_filters + filters['auxIdentifierFields']
                        else:
                            filters['auxIdentifierFields'] = []
                        filtered_data['baseDescription'] = filters['baseDescription']
                    else:
                        continue
                else:
                    filtered_data = data

                if filtered_data.empty == False:

                    filter_fields = [x for x in self.request_filter_fields if x['filterName'] == inst['filterName']][0]
                    sub_process_fields = list(dict(field_name=x['fieldName'], sub_processing_method=x['subProcessingMethod']) for x in filter_fields['values'] if x['subProcessingMethod'])
                    for pf in sub_process_fields:
                        try:
                            filtered_data[pf['field_name']] = filtered_data.apply(lambda x: eval(pf['sub_processing_method']), axis=1)
                        except:
                            continue

                    quotes = self.transform_data_to_quote_bean(filtered_data, filters['baseDescription'],
                                                               filters['idFields'] + filters[
                                                               'auxIdentifierFields'],
                                                               filters['identifierFields'],
                                                               filters['bidQuoteField'], filters['askQuoteField'],
                                                               filters['quoteType'], filters['quoteScalingFactor'], filters['auxFields'])
                    processing_time = time.time() - upload_time
                    print(filters['filterName'])
                    p.log.info(filters['filterName'])
                    status = self.upload_quotes_to_ov(filters['filterName'], quotes)
                    upload_time = time.time() - processing_time
                else:
                    print('No data found for', filters['filterName'])
                    p.log.warning('No data found for ' + filters['filterName'])

        return status


