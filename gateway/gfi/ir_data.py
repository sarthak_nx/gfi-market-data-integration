import pandas as pd, numpy as np, time
import properties as p
from gateway import properties as ov_p
from gateway.gfi.data_processor import GFI_DATA
from gateway.utils import ov_reference_utils as utils

class GFI_IR_RATES(GFI_DATA):

    def __init__(self, ftp_object, path, sub_directory, file_name):
        p.log.debug('Initializing.')
        super().__init__(ftp_object, path, sub_directory, file_name)

    def filter_data(self, data):

        if data.empty == False:

            status = None
            data = data.dropna(axis=0, how='any')

            data = self.create_date_time_fields(data)

            cleaning_time = time.time() - self.start_time
            upload_time = cleaning_time

            bid_filter = list(filter(lambda x: 'Bid' in x['filterName'], self.request_filters))[0]['values'][0]
            ask_filter = list(filter(lambda x: 'Ask' in x['filterName'], self.request_filters))[0]['values'][0]

            ask_data = data[data[ask_filter['dataFilterField']].str.contains(ask_filter['dataFilterValue'])]
            ask_data[self.primary_quote_identifier] = ask_data[self.primary_quote_identifier].str.replace('.A', '', regex = False)
            ask_data[ov_p.OV_UNIQUE_ID_FIELD] = ask_data[ask_filter['idFields']].apply(lambda x: '||'.join(x.dropna().astype(str).astype(str)), axis=1)

            bid_data = data[data[bid_filter['dataFilterField']].str.contains(bid_filter['dataFilterValue'])]
            bid_data[self.primary_quote_identifier] = bid_data[self.primary_quote_identifier].str.replace('.B', '', regex=False)
            bid_data[ov_p.OV_UNIQUE_ID_FIELD] = bid_data[bid_filter['idFields']].apply(lambda x: '||'.join(x.dropna().astype(str).astype(str)), axis=1)

            filter_data = pd.merge(bid_data, ask_data, on=self.primary_quote_identifier, how='outer', suffixes=['', '_ask'])

            temp_filter_data = []
            for row in filter_data.to_dict(orient = 'records'):
                temp_row = {}
                for k, v in row.items():
                    if isinstance(v, float) and np.isnan(v) and '_ask' in k:
                        temp_row[k] = row[k.replace('_ask', '')]
                    elif isinstance(v, float) and np.isnan(v) and '_ask' not in k:
                        temp_row[k] = row[k + '_ask']
                    else:
                        temp_row[k] = row[k]
                temp_filter_data.append(temp_row)

            filter_data = pd.DataFrame(temp_filter_data)

            data_filters = [ov_p.OV_QUOTE_DATE_TIME_FIELD]
            data_filters = data_filters + bid_filter['idFields'] + [bid_filter['bidQuoteField'], bid_filter['askQuoteField']]
            filtered_data = filter_data
            filtered_data['baseDescription'] = self.request_url.upper().replace('/','').replace('-','')

            if bid_filter['auxIdentifierFields']:
                for x, y in zip(bid_filter['auxIdentifierFields'], bid_filter['auxIdentifierValue']):
                    filtered_data[x] = y

                data_filters = data_filters + bid_filter['auxIdentifierFields']
            else:
                bid_filter['auxIdentifierFields'] = []

            if filtered_data.empty == False:

                filter_fields = [x for x in self.request_filter_fields if x['filterName'] == bid_filter['filterName']][0]
                sub_process_fields = list(
                    dict(field_name=x['fieldName'], sub_processing_method=x['subProcessingMethod']) for x in
                    filter_fields['values'] if x['subProcessingMethod'])
                for pf in sub_process_fields:
                    try:
                        filtered_data[pf['field_name']] = filtered_data.apply(lambda x: eval(pf['sub_processing_method']), axis=1)
                    except Exception as error:
                        p.log.exception(error)
                        continue

                quotes = self.transform_data_to_quote_bean(filtered_data, self.request_url.upper().replace('/','').replace('-',''),
                                                           bid_filter['idFields'],
                                                           bid_filter['identifierFields'] + bid_filter[
                                                               'auxIdentifierFields'],
                                                           bid_filter['bidQuoteField'], bid_filter['askQuoteField'],
                                                           bid_filter['quoteType'], bid_filter['quoteScalingFactor'], bid_filter['auxFields'])
                processing_time = time.time() - upload_time
                status = self.upload_quotes_to_ov(self.request_url.upper().replace('/','').replace('-',''), quotes)
                upload_time = time.time() - processing_time
            else:
                print('No data found')
                p.log.warning('No data found')

        else:
            print('No matching data found in', self.file_name)
            status = True

        return status