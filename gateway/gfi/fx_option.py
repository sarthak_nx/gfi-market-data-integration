import pandas as pd, time
import properties as p
from gateway import properties as ov_p
from gateway.gfi.data_processor import GFI_DATA


class FX_OPTION(GFI_DATA):

    def __init__(self, ftp_object, path, sub_directory, file_name):
        p.log.debug('Initializing.')
        super().__init__(ftp_object, path, sub_directory, file_name)

    def filter_data(self, data):

        status = None
        data = self.create_date_time_fields(data)

        cleaning_time = time.time() - self.start_time
        upload_time = cleaning_time

        bid_filter = list(filter(lambda x: 'Bid' in x['filterName'], self.request_filters))[0]['values'][0]
        ask_filter = list(filter(lambda x: 'Ask' in x['filterName'], self.request_filters))[0]['values'][0]

        ask_data = data[data[ask_filter['dataFilterField']].str.contains(ask_filter['dataFilterValue'])]
        ask_data[ov_p.OV_UNIQUE_ID_FIELD] = ask_data[ask_filter['idFields'] + ask_filter['identifierFields']].apply(lambda x: '||'.join(x.dropna().astype(str).astype(str)), axis=1)

        bid_data = data[data[bid_filter['dataFilterField']].str.contains(bid_filter['dataFilterValue'])]
        bid_data[ov_p.OV_UNIQUE_ID_FIELD] = bid_data[bid_filter['idFields'] + bid_filter['identifierFields']].apply(lambda x: '||'.join(x.dropna().astype(str).astype(str)), axis=1)

        filter_data = pd.merge(bid_data, ask_data, on=ov_p.OV_UNIQUE_ID_FIELD, how='outer', suffixes=['', '_y'])

        data_filters = [ov_p.OV_QUOTE_DATE_TIME_FIELD, ov_p.OV_QUOTE_DATE_FIELD]
        data_filters = data_filters + bid_filter['identifierFields'] + [bid_filter['bidQuoteField'], bid_filter['askQuoteField']]
        filtered_data = filter_data
        filtered_data['baseDescription'] = bid_filter['baseDescription']

        if bid_filter['auxIdentifierFields']:
            for x, y in zip(bid_filter['auxIdentifierFields'], bid_filter['auxIdentifierValue']):
                filtered_data[x] = y

            data_filters = data_filters + bid_filter['auxIdentifierFields']
        else:
            bid_filter['auxIdentifierFields'] = []

        if filtered_data.empty == False:
            quotes = self.transform_data_to_quote_bean(filtered_data,bid_filter['baseDescription'], bid_filter['idFields'],
                                                       bid_filter['identifierFields'] + bid_filter['auxIdentifierFields'],
                                                       bid_filter['bidQuoteField'], bid_filter['askQuoteField'],
                                                       bid_filter['quoteType'], bid_filter['quoteScalingFactor'], bid_filter['auxFields'])
            processing_time = time.time() - upload_time
            status = self.upload_quotes_to_ov('FXVOL',quotes)
            upload_time = time.time() - processing_time
        else:
            print('No data found')
            p.log.warning('No data found')

        return status