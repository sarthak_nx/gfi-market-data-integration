import openpyxl
import json, ast

from gateway.utils import properties

class ExcelToJson:

    filename = ''
    json_filename = ''

    def excel_to_json(self, filename, json_filename=None, columns=None, skip_columns=None):
        wb = openpyxl.load_workbook(filename)
        ws = wb[properties.SOURCE_REQUEST_TAB]
        rows = list(ws.rows)

        if len(rows) == 0:
            raise Exception('Sheet is empty')
        header = [c.value for c in rows[0]]
        if columns is None:
            columns = header

        if skip_columns is not None:
            for c in skip_columns:
                if c in columns:
                    columns.remove(c)

        result = []
        for row in rows[1:]:
            if len(header) != len(row):
                raise Exception('Invalid excel file. The number of headers and the number of columns are different.')
            m = {h: v.value for h, v in zip(header, row) if h in columns}

            row_item = {}
            for k, v in m.items():

                if isinstance(v, str) and '[' in v and ']' in v:
                    row_item[k] = ast.literal_eval(v)
                else:
                    row_item[k] = v

            row_item['requestTypes'] = self.excel_to_json_helper(filename, properties.SOURCE_REQUEST_TYPE_TAB,
                                                                  None, row_item, row_item['requestName'], 'requestType')

            row_item['requestScope'] = self.excel_to_json_helper(filename, properties.SOURCE_EXTERNAL_TICKER_TAB,
                                                                 None, row_item, row_item['requestName'], 'requestGroup')
            row_item['requestFilterFields'] = self.excel_to_json_helper(filename, properties.SOURCE_REQUEST_FILTER_FIELDS_TAB,
                                                                 None, row_item, row_item['requestName'], 'filterName')
            row_item['requestFilter'] = self.excel_to_json_helper(filename, properties.SOURCE_REQUEST_FILTER_TAB,
                                                                 None, row_item, row_item['requestName'], 'filterName')
            # print('Processed request %s, sub-request %s' % (str(row_item['requestName']), str(row_item['subRequestName'])))
            result.append(row_item)
        # print('Processed sheets %s, %s, %s' % (properties.SOURCE_REQUEST_TYPE_TAB, properties.SOURCE_EXTERNAL_TICKER_TAB, properties.SOURCE_REQUEST_FILTER_TAB))
        if json_filename:
            with open(json_filename, 'w') as f:
                json.dump(result, f, indent=2)
        return result

    def excel_to_json_helper(self, filename, sheet_name='', columns=None, sheet_row = None, type = '', field = ''):
        wb = openpyxl.load_workbook(filename)
        ws = wb[sheet_name]
        rows = list(ws.rows)

        if len(rows) == 0:
            raise Exception('Sheet is empty')
        header = [c.value for c in rows[0]]
        if columns is None:
            columns = header

        result = []
        fields = []
        for row in rows[1:]:
            if len(header) != len(row):
                raise Exception('Invalid excel file. The number of headers and the number of columns are different.')
            m = {h: v.value for h, v in zip(header, row) if h in columns}
            try:
                if m['requestName'] + str(m['subRequestName']) == sheet_row['requestName'] + str(sheet_row['subRequestName']):
                    result.append(m)
                    fields.append(m[field])
            except:
                pass
        fields = set(fields)

        results = []
        for f in fields:
            dict = {}
            dict[field] = f
            dict['values'] = []
            for v_row in list(filter(lambda x: x[field] == f, result)):
                dict_2 = {}
                for k, v in v_row.items():
                    if isinstance(v, str) and '[' in v and ']' in v:
                        try:
                            dict_2[k] = ast.literal_eval(v)
                        except:
                            dict_2[k] = v
                    else:
                        dict_2[k] = v
                dict['values'].append(dict_2)
            results.append(dict)

        return results


