import requests, json
import sys, os
from gateway import properties as ov_p
from gateway import env_setting as env
from multiprocessing.dummy import Pool as ThreadPool
import properties as p
from datetime import datetime

import pandas as pd, numpy as np

def get_underlying_ext_name_setup(filters):

    response = requests.get(ov_p.OV_BASE_URL + '/market-gateway/underlying-ext-name',
                            headers={'Authorization': ov_p.OV_TOKEN,
                                     'Content-Type': 'application/json'},
                            params=dict(criteria=json.dumps(dict(filters=filters))))
    json_response = json.loads(response.text)
    return json_response['data']

def split_string(x, column, sep):
    try:
        y = x[column].split(sep)
        return [z.strip() for z in y]
    except:
        try:
            return x[column] * len(x)
        except:
            return [np.nan] * len(x)

def get_substring(x, column, start, end):

    try:
        y = x[column][start:end]
        return y
    except:
        try:
            return x[column]
        except:
            return [np.nan]

def passthrough_column(x, column):

    try:
        y = x[column]
        return y
    except:
        return [np.nan]

def quote_upload(file_name='Numerix', quotes=None, use_mapping=True):
    check = 1
    if len(quotes) > 0:

        chunk_size = 1000
        quotes_chunks = [quotes[offs:offs + chunk_size] for offs in range(0, len(quotes), chunk_size)]

        request_chunks = []
        now = datetime.now().strftime('%Y%m%d%H%M%S')
        quotes_df = pd.DataFrame(quotes)
        TODAY = datetime.now().strftime('%Y-%m-%d')
   
        output_path = os.path.join(p.OUTPUT_PATH, TODAY)
        
        if not os.path.exists(output_path):
            os.makedirs(output_path)
        
        quotes_df.to_csv(os.path.join(output_path, file_name + '_' + now + '.csv.gz'), index = False, compression='gzip')

        # for quote_chunk in quotes_chunks:
        #
        #     req_pld = {}
        #     req_pld['source'] = ov_p.OV_QUOTE_SOURCE
        #     req_pld['dataType'] = file_name
        #     req_pld['data'] = quote_chunk
        #
        #     request_chunks.append(req_pld)
        #
        # status = parallel_process(request_chunks, threads=5)
        return bool(check)
    else:
        return bool(check)

def parallel_process(data_list, threads = 5):

    pool = ThreadPool(threads)
    status = pool.map(push_quotes, data_list)
    return status

def push_quotes(request_payload = None):

    response = requests.put(env.OV_BASE_URL + env.url,
                             headers={'Content-Type': 'application/json'},
                             data=json.dumps(request_payload, default=str))
    try:

        if response.status_code == 200:
            response_data = json.loads(response.text)
            print(response_data['message'])
            p.log.info(response_data['message'])
        else:
            response_data = json.loads(response.text)
            print(response_data)
            p.log.error(response_data)
    except:
        print('Could not parse API response.')
        p.log.warning('Could not parse API response.')