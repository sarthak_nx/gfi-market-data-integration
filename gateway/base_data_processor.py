import os, sys, json, time
import pandas as pd
import numpy as np
import time

import properties as p
from datetime import datetime
from pytz import timezone
# from gateway.gfi import properties as p
from gateway import properties as ov_p
from gateway.utils.ov_reference_utils import quote_upload

from gateway.utils import ov_reference_utils as utils
from multiprocessing.dummy import Pool as ThreadPool
pd.options.mode.chained_assignment = None

import gzip
from io import StringIO

class BaseDataProcesser:

    def __init__(self, ftp_object,  path = None, sub_directory = None,  file_name = None):

        self.ftp_object = ftp_object
        self.request_url = path
        self.file_name = file_name
        self.sub_directory = sub_directory
        self.start_time = time.time()


    def initialize_request(self, request_class, sub_request_class, request_type, request_group):

        self.request_class = request_class
        self.sub_request_class = sub_request_class
        self.request_type = request_type
        self.request_group = request_group

    def load_request_mapping(self):

        with open(os.path.join(ov_p.MAPPING_CONFIG_PATH, ov_p.MAPPING_CONFIG_JSON_NAME), 'r') as rf:
            request_mapping = json.load(rf)
            rf.close()

        try:
            request_mapping = list(filter(lambda x: x['requestName'] == self.request_class, request_mapping))
        except IndexError:
            print('Invalid request %s' %self.request_class)
            p.log.error('Invalid request %s' % self.request_class)
            sys.exit()

        try:
            request_mapping = list(filter(lambda x: x['subRequestName'] == self.sub_request_class, request_mapping))[0]
            self.primary_quote_identifier = request_mapping['primaryQuoteIdentifier']
            self.quote_date_time_field = request_mapping['quoteDatetimeField']
            self.ext_source = request_mapping['source']
            self.date_time_zone = request_mapping['quoteTimeZone']
            self.date_format_1 = request_mapping['quoteDatetimeFormat1']
            self.date_format_2 = request_mapping['quoteDatetimeFormat2']

        except IndexError:
            print('Invalid request %s' %self.request_class)
            p.log.error('Invalid request %s' % self.request_class)
            sys.exit()

        try:
            self.columns = request_mapping['dataHeaders']
            request_type_mapping = list(filter(lambda x: x['requestType'] == self.request_type, request_mapping['requestTypes']))[0]
            self.request_fields = list(x['fieldName'] for x in request_type_mapping['values'])
            self.sub_process_fields = list(dict(field_name = x['fieldName'], sub_processing_method = x['subProcessingMethod']) for x in request_type_mapping['values'] if x['subProcessingMethod'])
        except IndexError:
            print('Invalid request type %s' % self.request_type)
            p.log.error('Invalid request type %s' % self.request_type)
            sys.exit()

        try:
            self.request_filter_fields = request_mapping['requestFilterFields']
        except:
            print('Invalid request filters')
            p.log.error('Invalid request filters')
            pass

        try:
            self.request_filters = request_mapping['requestFilter']
        except:
            print('Invalid request filters')
            p.log.error('Invalid request filters')
            pass

    def load_data(self):

        try:
            if self.sub_directory:
                self.ftp_object.cwd(self.request_url + self.sub_directory)
            else:
                self.ftp_object.cwd(self.request_url)

            data_bytes = bytearray()
            def handle_binary(more_data):
                data_bytes.extend(more_data)

            p.log.debug('Reading file ' + self.file_name)
            self.ftp_object.retrbinary("RETR " + self.file_name, callback=handle_binary)
            self.file_size = self.ftp_object.size(self.file_name)
            print('Size', "{0:.4f}".format(self.file_size / 1048576), 'mb')
            p.log.debug('Size ' + "{0:.4f}".format(self.file_size / 1048576) + 'mb')
            data_orig = pd.read_csv(StringIO(str(data_bytes, 'utf-8')))

            data = data_orig

            if data.empty == False:
                data = data.dropna(axis=1, how='all')
                new_data = pd.DataFrame([], columns=list(data.iloc[0]))
                data = data.iloc[1:]
                data.columns = new_data.columns
                new_data = new_data.append(data, ignore_index=True)
                return new_data
            else:
                return pd.DataFrame([])
        except TimeoutError:
            print('FTP connection timedout')
            p.log.exception('FTP connection timedout')
            return pd.DataFrame([])
        except Exception as error:
            print('Error occured while reading FTP file')
            p.log.error('Error occured while reading FTP file')
            p.log.exception(error)
            return pd.DataFrame([])

    def load_data_local(self):

        data_orig = pd.read_csv(os.path.join(self.request_url, self.file_name))

        data = data_orig

        if data.empty == False:
            data = data.dropna(axis=1, how='all')
            new_data = pd.DataFrame([], columns=list(data.iloc[0]))
            data = data.iloc[1:]
            data.columns = new_data.columns
            new_data = new_data.append(data, ignore_index=True)
            return new_data
        else:
            return pd.DataFrame([])


    def get_filtered_data(self):

        loaded_data = self.load_data()
        # loaded_data = self.load_data_local()
        print('1',len(loaded_data))

        # loaded_data = loaded_data.dropna(axis=0, thresh=int(len(loaded_data.columns))/2-1)
        loaded_data = loaded_data.dropna(axis=0, subset=[self.primary_quote_identifier])
        loaded_data = loaded_data[loaded_data[self.primary_quote_identifier] != 'EOF']
        print('2',len(loaded_data))
        for pf in self.sub_process_fields:
            loaded_data[pf['field_name']] = loaded_data.apply(lambda x: eval(pf['sub_processing_method']), axis = 1)

        print('3',len(loaded_data))
        try:
            data = loaded_data[self.request_fields]
        except:
            data = loaded_data
        print('4',len(data))
        data = data.dropna(axis=0, how = 'all')
        print('5',len(data))
        return data


    def localize_quote_datetime_old(self, x):
        try:
            x = datetime.timestamp(timezone(self.date_time_zone).localize(datetime.strptime(x, self.date_format_1)).astimezone(timezone(ov_p.OV_QUOTE_TIME_ZONE)))

        except:
            try:
                x = datetime.timestamp(timezone(self.date_time_zone).localize(datetime.strptime(x, self.date_format_2)).astimezone(timezone(ov_p.OV_QUOTE_TIME_ZONE)))
            except:
                x = np.nan
        return x

    def localize_quote_datetime(self, x):
        try:
            x = datetime.timestamp(timezone(self.date_time_zone).localize(datetime.strptime(x, self.date_format_1)).astimezone(timezone(ov_p.OV_QUOTE_TIME_ZONE)))

        except:
            try:
                x = datetime.timestamp(timezone(self.date_time_zone).localize(datetime.strptime(x, self.date_format_2)).astimezone(timezone(ov_p.OV_QUOTE_TIME_ZONE)))
            except:
                x = np.nan
        return x

    def datetime_to_date_str(self,x):

        try:
            x = datetime.fromtimestamp(x).strftime(ov_p.OV_QUOTE_DATE_FORMAT)
        except:
            x = np.nan

        return x

    def create_date_time_fields(self,data):

        data[ov_p.OV_QUOTE_DATE_TIME_FIELD] = (data[self.quote_date_time_field].apply(lambda x: ' '.join(x.dropna().astype(str).astype(str)), axis=1))
        data = data[(data[ov_p.OV_QUOTE_DATE_TIME_FIELD].isnull() == False)]

        try:
            data[ov_p.OV_QUOTE_DATE_TIME_FIELD]  = pd.to_datetime(data[ov_p.OV_QUOTE_DATE_TIME_FIELD], format  = self.date_format_1, errors='raise').dt.tz_localize(self.date_time_zone).dt.tz_convert(ov_p.OV_QUOTE_TIME_ZONE)
        except:
            data[ov_p.OV_QUOTE_DATE_TIME_FIELD] = pd.to_datetime(data[ov_p.OV_QUOTE_DATE_TIME_FIELD],format=self.date_format_2,errors='coerce').dt.tz_localize(self.date_time_zone).dt.tz_convert(ov_p.OV_QUOTE_TIME_ZONE)
        data[ov_p.OV_QUOTE_DATE_FIELD] =data[ov_p.OV_QUOTE_DATE_TIME_FIELD].dt.strftime(date_format = ov_p.OV_QUOTE_DATE_FORMAT)
        data[ov_p.OV_QUOTE_DATE_TIME_FIELD] = data[ov_p.OV_QUOTE_DATE_TIME_FIELD].astype('int64') // 10**9
        return data

    def create_unique_fields(self,data):

        data[self.description_id_fields + self.unique_id_fields] = data[self.description_id_fields + self.unique_id_fields].dropna().astype(str)

        data[ov_p.OV_QUOTE_INSTRUMENT_TYPE_FIELD] = self.base_description

        if len(self.description_id_fields) == 1:
            data[ov_p.OV_UNIQUE_ID_FIELD] = data[self.description_id_fields]
        else:
            data[ov_p.OV_UNIQUE_ID_FIELD] = ''
            for f in self.description_id_fields:
                data[ov_p.OV_UNIQUE_ID_FIELD] = data[ov_p.OV_UNIQUE_ID_FIELD] + data[f] + '||'

        data['IDENTIFIER_FIELD'] = ''
        for f in self.unique_id_fields:
            data['IDENTIFIER_FIELD'] = data['IDENTIFIER_FIELD'] + data[f] + '||'

        # if len(self.description_id_fields) ==1:
        #     data[ov_p.OV_UNIQUE_IDENTIFIER_FIELD] = data[ov_p.OV_QUOTE_INSTRUMENT_TYPE_FIELD] + '||' + data[ov_p.OV_UNIQUE_ID_FIELD] + '||' + data['IDENTIFIER_FIELD']
        # elif len(self.description_id_fields) !=1:
        #     data[ov_p.OV_UNIQUE_IDENTIFIER_FIELD] = data[ov_p.OV_QUOTE_INSTRUMENT_TYPE_FIELD] + '||' + data[ov_p.OV_UNIQUE_ID_FIELD] + data['IDENTIFIER_FIELD']

        if len(self.description_id_fields) ==1:
            data[ov_p.OV_UNIQUE_IDENTIFIER_FIELD] = data[ov_p.OV_UNIQUE_ID_FIELD] + '||' + data['IDENTIFIER_FIELD']
        elif len(self.description_id_fields) !=1:
            data[ov_p.OV_UNIQUE_IDENTIFIER_FIELD] = data[ov_p.OV_UNIQUE_ID_FIELD] + data['IDENTIFIER_FIELD']


        return data

    def process_request(self, request_class, sub_request_class, request_type, request_group):

        status = self.get_quotes_from_data(request_class, sub_request_class, request_type, request_group)
        return status, self.file_size

    def get_quotes_from_data(self, request_class, sub_request_class, request_type, request_group):

        self.initialize_request(request_class, sub_request_class, request_type, request_group)
        self.load_request_mapping()
        data = self.get_filtered_data()

        status = self.filter_data(data)
        return status

    def filter_data(self, data):

        status = None

        data = self.create_date_time_fields(data)

        cleaning_time = time.time() - self.start_time
        upload_time = cleaning_time
        for inst in self.request_filters:
            filters = inst['values'][0]

            data_filters = [ov_p.OV_QUOTE_DATE_TIME_FIELD, ov_p.OV_QUOTE_DATE_FIELD]
            data_filters = data_filters + filters['identifierFields'] + [filters['bidQuoteField'],filters['askQuoteField']]
            if set(data_filters).issubset(data.columns):

                if filters['dataFilterField']:
                    # if isinstance(filters['dataFilterField'], list):
                    #     filtered_data = data[data[filters['dataFilterField']].isin(filters['dataFilterValue'])]
                    if isinstance(filters['dataFilterValue'], list):
                        filtered_data = pd.DataFrame([])
                        for filter_value in filters['dataFilterValue']:
                            filtered_data = pd.concat([filtered_data, data[data[filters['dataFilterField']].str.contains(filter_value)]], axis=0, ignore_index=True, sort=True)
                    else:
                        filtered_data = data[data[filters['dataFilterField']].str.contains(filters['dataFilterValue'])]
                else:
                    filtered_data = data

                if filters['excDataFilterValue']:
                    filtered_data = filtered_data[filtered_data[filters['dataFilterField']].str.contains('|'.join(filters['excDataFilterValue'])) == False]
                filtered_data['baseDescription'] = filters['baseDescription']
                if filters['auxIdentifierFields']:
                    for x, y in zip(filters['auxIdentifierFields'], filters['auxIdentifierValue']):
                        filtered_data[x] = y

                    data_filters = data_filters + filters['auxIdentifierFields']
                else:
                    filters['auxIdentifierFields'] = []

                if filtered_data.empty == False:

                    filter_fields = [x for x in self.request_filter_fields if x['filterName'] == inst['filterName']][0]
                    sub_process_fields = list(dict(field_name=x['fieldName'], sub_processing_method=x['subProcessingMethod']) for x in filter_fields['values'] if x['subProcessingMethod'])
                    for pf in sub_process_fields:
                        try:
                            filtered_data[pf['field_name']] = filtered_data.apply(lambda x: eval(pf['sub_processing_method']), axis=1)
                        except Exception as error:
                            p.log.exception(error)
                            continue

                    quotes = self.transform_data_to_quote_bean(filtered_data, filters['baseDescription'],
                                                               filters['idFields'],
                                                               filters['identifierFields'] + filters[
                                                                   'auxIdentifierFields'],
                                                               filters['bidQuoteField'], filters['askQuoteField'],
                                                               filters['quoteType'], filters['quoteScalingFactor'], filters['auxFields'])
                    processing_time = time.time() - upload_time
                    print(filters['filterName'])
                    p.log.info(filters['filterName'])
                    status = self.upload_quotes_to_ov(filters['filterName'], quotes)
                    upload_time = time.time() - processing_time
                else:
                    print('No data found for', filters['filterName'])
                    p.log.warning('No data found for ' + filters['filterName'])

        return status

    def transform_data_to_quote_bean_old(self, data, base_description, description_id_fields, unique_id_fields, bid_quote_field, ask_quote_field, quote_type, quote_conv_fact):

        if bid_quote_field == ask_quote_field:

            quote_beans = self.create_quote_bean(data, base_description, description_id_fields, unique_id_fields, bid_quote_field, 'MID', quote_type, quote_conv_fact)

        else:

            quote_beans = pd.DataFrame()
            bid_quote_beans = self.create_quote_bean(data, base_description, description_id_fields, unique_id_fields, bid_quote_field, 'BID', quote_type, quote_conv_fact)
            ask_quote_beans = self.create_quote_bean(data, base_description, description_id_fields, unique_id_fields, ask_quote_field, 'ASK', quote_type, quote_conv_fact)

            quote_beans = quote_beans.append(bid_quote_beans, ignore_index=True)
            quote_beans = quote_beans.append(ask_quote_beans, ignore_index=True)

        quote_beans = quote_beans.drop_duplicates([ov_p.OV_UNIQUE_ID_FIELD, ov_p.OV_UNIQUE_IDENTIFIER_FIELD, ov_p.OV_QUOTE_DATE_TIME_FIELD,ov_p.OV_QUOTE_SIDE_FIELD])
        quote_beans = quote_beans.sort_values([ov_p.OV_UNIQUE_ID_FIELD, ov_p.OV_UNIQUE_IDENTIFIER_FIELD, ov_p.OV_QUOTE_DATE_TIME_FIELD,ov_p.OV_QUOTE_SIDE_FIELD])
        return quote_beans

    def create_quote_bean_old(self, data, base_description, description_id_fields, unique_id_fields, quote_field, quote_side, quote_type, quote_conv_fact):

        clean_data = data[(data[quote_field].isnull() == False)]

        self.base_description = base_description
        self.description_id_fields = description_id_fields
        self.unique_id_fields = unique_id_fields
        clean_data = self.create_unique_fields(clean_data)

        etl_data = pd.DataFrame()
        etl_data[ov_p.OV_QUOTE_INSTRUMENT_TYPE_FIELD] = clean_data[ov_p.OV_QUOTE_INSTRUMENT_TYPE_FIELD]
        etl_data[ov_p.OV_UNIQUE_ID_FIELD] = clean_data[ov_p.OV_UNIQUE_ID_FIELD]
        etl_data[ov_p.OV_UNIQUE_IDENTIFIER_FIELD] = clean_data[ov_p.OV_UNIQUE_IDENTIFIER_FIELD]
        etl_data[ov_p.OV_QUOTE_DATE_FIELD] = clean_data[ov_p.OV_QUOTE_DATE_FIELD]
        etl_data[ov_p.OV_QUOTE_DATE_TIME_FIELD] = clean_data[ov_p.OV_QUOTE_DATE_TIME_FIELD]

        etl_data[ov_p.OV_QUOTE_FIELD] = clean_data[quote_field].astype(float) / quote_conv_fact
        etl_data = etl_data[(etl_data[ov_p.OV_QUOTE_FIELD].isnull() == False)]
        etl_data[ov_p.OV_QUOTE_SIDE_FIELD] = quote_side

        etl_data[ov_p.OV_QUOTE_UPLOAD_TIME] = int(datetime.timestamp(datetime.now(tz=timezone('UTC')).replace(microsecond=0)))

        return etl_data

    def transform_data_to_quote_bean(self, data, base_description, description_id_fields, unique_id_fields, bid_quote_field, ask_quote_field, quote_type, quote_conv_fact, aux_fields = None):

        # if bid_quote_field == ask_quote_field:
        #
        #     quote_beans = self.create_quote_bean(data, base_description, description_id_fields, unique_id_fields, bid_quote_field, 'MID', quote_type, quote_conv_fact)
        #
        # else:
        #
        #     quote_beans = pd.DataFrame()
        #     bid_quote_beans = self.create_quote_bean(data, base_description, description_id_fields, unique_id_fields, bid_quote_field, 'BID', quote_type, quote_conv_fact)
        #     ask_quote_beans = self.create_quote_bean(data, base_description, description_id_fields, unique_id_fields, ask_quote_field, 'ASK', quote_type, quote_conv_fact)
        #
        #     quote_beans = quote_beans.append(bid_quote_beans, ignore_index=True)
        #     quote_beans = quote_beans.append(ask_quote_beans, ignore_index=True)
        quote_beans = self.create_quote_bean(data, base_description, description_id_fields, unique_id_fields, bid_quote_field, ask_quote_field, quote_type, quote_conv_fact, aux_fields)
        quote_beans = quote_beans.drop_duplicates([ov_p.OV_UNIQUE_ID_FIELD, ov_p.OV_UNIQUE_IDENTIFIER_FIELD, ov_p.OV_QUOTE_DATE_TIME_FIELD,ov_p.OV_QUOTE_TYPE_FIELD])
        quote_beans = quote_beans.sort_values([ov_p.OV_UNIQUE_ID_FIELD, ov_p.OV_UNIQUE_IDENTIFIER_FIELD, ov_p.OV_QUOTE_DATE_TIME_FIELD,ov_p.OV_QUOTE_TYPE_FIELD])
        return quote_beans

    def create_quote_bean(self, data, base_description, description_id_fields, unique_id_fields, bid_quote_field, ask_quote_field, quote_type, quote_conv_fact, aux_fields = None):

        clean_data = data[(data[bid_quote_field].isnull() == False) & (data[ask_quote_field].isnull() == False)]

        self.base_description = base_description
        self.description_id_fields = description_id_fields
        self.unique_id_fields = unique_id_fields
        clean_data = self.create_unique_fields(clean_data)

        etl_data = pd.DataFrame()
        etl_data[ov_p.OV_QUOTE_INSTRUMENT_TYPE_FIELD] = clean_data[ov_p.OV_QUOTE_INSTRUMENT_TYPE_FIELD]
        etl_data[ov_p.OV_UNIQUE_ID_FIELD] = clean_data[ov_p.OV_UNIQUE_ID_FIELD]
        etl_data[ov_p.OV_UNIQUE_IDENTIFIER_FIELD] = clean_data[ov_p.OV_UNIQUE_IDENTIFIER_FIELD]
        etl_data[ov_p.OV_QUOTE_DATE_FIELD] = clean_data[ov_p.OV_QUOTE_DATE_FIELD]
        etl_data[ov_p.OV_QUOTE_DATE_TIME_FIELD] = clean_data[ov_p.OV_QUOTE_DATE_TIME_FIELD]

        etl_data[ov_p.OV_BID_QUOTE_FIELD] = clean_data[bid_quote_field].astype(float) / quote_conv_fact
        etl_data[ov_p.OV_ASK_QUOTE_FIELD] = clean_data[ask_quote_field].astype(float) / quote_conv_fact
        etl_data = etl_data[(etl_data[ov_p.OV_BID_QUOTE_FIELD].isnull() == False) & (etl_data[ov_p.OV_ASK_QUOTE_FIELD].isnull() == False)]
        etl_data[ov_p.OV_QUOTE_TYPE_FIELD] = quote_type

        etl_data[ov_p.OV_QUOTE_UPLOAD_TIME] = int(datetime.timestamp(datetime.now(tz=timezone('UTC')).replace(microsecond=0)))

        if aux_fields:
            etl_data[aux_fields] = clean_data[aux_fields]

        return etl_data

    def upload_quotes_to_ov(self, name, data):

        data_list = data.to_dict(orient = 'records')
        print('Uploading', len(data_list), 'quotes.')
        p.log.debug('Uploading ' + str(len(data_list)) + ' quotes.')
        status = quote_upload(name, data_list)

        # return status

