import os
from gateway import properties as ov_p
from gateway.utils.request_group_excel_to_json import ExcelToJson

xl = ExcelToJson()
request_mapping = xl.excel_to_json(os.path.join(ov_p.MAPPING_CONFIG_PATH, ov_p.MAPPING_CONFIG_XL_NAME), os.path.join(ov_p.MAPPING_CONFIG_PATH, ov_p.MAPPING_CONFIG_JSON_NAME))
