import json, os
from ftplib import FTP
from file_processor import FileProcessor
import properties as p
from multiprocessing.dummy import Pool as ThreadPool
import os
class FileHandler:

    def __init__(self, date):

        self.file_date = date

    def get_ftp_file_config(self):

        with open(os.path.join(p.CONFIG_PATH,p.FILE_TYPE_PROPERTIES), 'r') as fp:
            self.file_type_properties = json.load(fp)
            fp.close()

    def load_file_config(self, file_type):

        self.get_ftp_file_config()
        all_file_types = []
        for k, v in self.file_type_properties.items():
            all_file_types.extend(list(x for x in v if x['process']))

        self.file_property = [x for x in all_file_types if x['name'] == file_type ][0]


    def get_ftp_properties(self):

        with open(os.path.join(p.CONFIG_PATH,p.FTP_PROPERTIES), 'r') as fp:
            self.ftp_properties = json.load(fp)
            fp.close()

    def load_ftp_properties(self):

        self.get_ftp_properties()
        self.ftp_property = [x for x in self.ftp_properties if x['name'] == self.file_property['ftp']][0]
        print()

    def load_static_data(self):

        self.get_ftp_file_config()
        self.get_ftp_properties()

    def initialize_ftp(self):

        ftp = FTP(self.ftp_property['url'])

        if self.ftp_property['username'] and self.ftp_property['password']:

            ftp.login(user=self.ftp_property['username'], passwd=self.ftp_property['password'])

        return ftp

    def get_ftp_file_listing(self):

        self.load_static_data()

        threads = len(self.file_type_properties)

        config_list = []


        for k, v in self.file_type_properties.items():
            config_list.append(list(x for x in FileHandler.__subclasses__() if x.__name__ in [m['name'] for m in v if m['process']]))
        threads = 1

        self.calculate_parallel(config_list, threads)

    def calculate_parallel(self, data_list, threads=2):
        pool = ThreadPool(threads)
        pool.map(self.get_market_data, data_list)

    def get_market_data(self, file_types_to_process):

        for file_type in file_types_to_process:
            print()
            print('Triggered', file_type.__name__)
            p.log.info('Triggered ' +  file_type.__name__)
            file_type(self.file_date).file_handler()

    def file_handler(self):

        ftp = self.initialize_ftp()

        if self.file_property['sub_directories']:
            for d in self.file_property['sub_directories']:
                print(d)
                p.log.info(d)
                file_list = self.get_ftp_file_names(ftp, self.file_property['path'],  d + '/')
                self.dispatch_file_processing_request(ftp, self.file_property['path'], d + '/', file_list)
        else:
            file_list = self.get_ftp_file_names(ftp, self.file_property['path'], '')
            self.dispatch_file_processing_request(ftp, self.file_property['path'], '', file_list)


    def get_ftp_file_names(self, ftp, path, sub_directory):

        print('Finding files for', self.file_date.strftime('%Y-%m-%d'))
        p.log.info('Finding files for ' +  self.file_date.strftime('%Y-%m-%d'))
        if sub_directory:
            ftp.cwd(path + sub_directory)
        else:
            ftp.cwd(path)

        files = ftp.nlst()

        file_list = [x for x in files if self.file_date.strftime(self.file_property['file_datestamp']) in x]

        if self.file_property['file_keyword']:

            file_list = [x for x in file_list if self.file_property['file_keyword'] in x]

        print('Found', len(file_list), 'file(s)')
        p.log.info('Found ' +  str(len(file_list)) + ' file(s)')
        return file_list

    def get_local_dir_file_names(self, path):

        print('Finding files for', self.file_date.strftime('%Y-%m-%d'))
        p.log.info('Finding files for ' + self.file_date.strftime('%Y-%m-%d'))
        files = os.listdir(path)

        file_list = [x for x in files if self.file_date.strftime(self.file_property['file_datestamp']) in x]

        if self.file_property['file_keyword']:

            file_list = [x for x in file_list if self.file_property['file_keyword'] in x]

        print('Found', len(file_list), 'file(s)')
        p.log.info('Found ' + str(len(file_list)) + 'file(s)')
        return file_list

    def dispatch_file_processing_request(self, ftp, request_path, sub_directory, file_list):

        if os.path.exists(p.FILE_STATUS_PATH) == False:
            os.mkdir(p.FILE_STATUS_PATH)

        list_of_files = os.listdir(p.FILE_STATUS_PATH)

        if str(self.file_date.strftime("%Y-%m-%d")) + '.json' not in list_of_files:
            with open(os.path.join(p.FILE_STATUS_PATH,str(self.file_date.strftime("%Y-%m-%d")) + '.json'), 'w') as fs:
                json.dump([], fs, indent=4)
                fs.close()

        with open(os.path.join(p.FILE_STATUS_PATH, str(self.file_date.strftime("%Y-%m-%d")) + '.json'), 'r') as fs:
            file_status = json.load(fs)
            fs.close()

        for file in file_list:
            if file not in [x['name'] for x in file_status]:
                try:
                    print(file)
                    p.log.info(file)
                    f = FileProcessor(ftp_object=ftp, request_path = request_path, sub_directory = sub_directory, file_name=file)
                    status, file_size = f.processor()
                    if status:
                        if self.file_property['is_intraday'] == 'Yes':
                            with open(os.path.join(p.FILE_STATUS_PATH, str(self.file_date.strftime("%Y-%m-%d")) + '.json'), 'r') as fs:
                                file_status = json.load(fs)
                                fs.close()

                            file_status.append(dict(name = file, size = file_size))

                            with open(os.path.join(p.FILE_STATUS_PATH, str(self.file_date.strftime("%Y-%m-%d")) + '.json'), 'w') as fs:
                                json.dump(file_status, fs, indent=4)
                                fs.close()
                    print()
                except Exception as error:
                    print('Error processing file', file)
                    p.log.exception('Error processing file ' + file)
                    p.log.exception(error)
            else:
                print('Skipping file', file, '.Already Processed.')
                p.log.warning('Skipping file ' + file+ ' .Already Processed.')


class RPM_EMEA(FileHandler):

    def __init__(self, file_date):

        file_type_name = 'RPM_EMEA'
        self.load_file_config(file_type_name)
        self.load_ftp_properties()
        super().__init__(file_date)

class RPM_CHINA(FileHandler):

    def __init__(self, file_date):

        file_type_name = 'RPM_CHINA'
        self.load_file_config(file_type_name)
        self.load_ftp_properties()
        super().__init__(file_date)


class CCT_IR_RATES(FileHandler):

    def __init__(self, file_date):

        file_type_name = 'CCT_IR_RATES'
        self.load_file_config(file_type_name)
        self.load_ftp_properties()
        super().__init__(file_date)


class BGC_IR_RATES(FileHandler):

    def __init__(self, file_date):

        file_type_name = 'BGC_IR_RATES'
        self.load_file_config(file_type_name)
        self.load_ftp_properties()
        super().__init__(file_date)

class GFI_IR_BONDS(FileHandler):

    def __init__(self, file_date):

        file_type_name = 'GFI_IR_BONDS'
        self.load_file_config(file_type_name)
        self.load_ftp_properties()
        super().__init__(file_date)

class GFI_FX_OPTION(FileHandler):

    def __init__(self, file_date):

        file_type_name = 'GFI_FX_OPTION'
        self.load_file_config(file_type_name)
        self.load_ftp_properties()
        super().__init__(file_date)

class FMD_IR_RATES(FileHandler):

    def __init__(self, file_date):

        file_type_name = 'FMD_IR_RATES'
        self.load_file_config(file_type_name)
        self.load_ftp_properties()
        super().__init__(file_date)

class FMD_FX_RATES(FileHandler):

    def __init__(self, file_date):

        file_type_name = 'FMD_FX_RATES'
        self.load_file_config(file_type_name)
        self.load_ftp_properties()
        super().__init__(file_date)


class GFI_IR_OPTION(FileHandler):

    def __init__(self, file_date):

        file_type_name = 'GFI_IR_OPTION'
        self.load_file_config(file_type_name)
        self.load_ftp_properties()
        super().__init__(file_date)


class GFI_IRS(FileHandler):

    def __init__(self, file_date):

        file_type_name = 'GFI_IRS'
        self.load_file_config(file_type_name)
        self.load_ftp_properties()
        super().__init__(file_date)

#
class GFI_NDIRS(FileHandler):

    def __init__(self, file_date):

        file_type_name = 'GFI_NDIRS'
        self.load_file_config(file_type_name)
        self.load_ftp_properties()
        super().__init__(file_date)


class GFI_NDS(FileHandler):

    def __init__(self, file_date):

        file_type_name = 'GFI_NDS'
        self.load_file_config(file_type_name)
        self.load_ftp_properties()
        super().__init__(file_date)


class GFI_CR_CURVE(FileHandler):

    def __init__(self, file_date):

        file_type_name = 'GFI_CR_CURVE'
        self.load_file_config(file_type_name)
        self.load_ftp_properties()
        super().__init__(file_date)

class GFI_CR_INDEX(FileHandler):

    def __init__(self, file_date):

        file_type_name = 'GFI_CR_INDEX'
        self.load_file_config(file_type_name)
        self.load_ftp_properties()
        super().__init__(file_date)