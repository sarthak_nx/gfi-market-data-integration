import logging
import json
import requests
import time

try:
    # Debug mode might not work when using locust because of gevent.
    # You can enable it in pycharm settings under Build, Execution, Deployment >> Python Debugger >> Gevent compatible
    from locust import events
    LOCUST_INSTALLED = True
except ImportError:
    LOCUST_INSTALLED = False
    import sys
    print("locust is not currently available on this platform. Disabling locust features", file=sys.stderr)

log = logging.getLogger(__name__)

#Change this mode to '18.03' to work with pre-microservices version.
MODE = '18.06'

class OneViewPyClient():
    _instance = None

    # _request_log = list()

    # If multiple instances of the class need to be alive at any point, singleton should be set to false in the initialization of each of them.
    # If at anytime, OneViewPyClient() is called with singleton=True, all previous instances will be overridden and only the singleton would live
    def __new__(cls, singleton=True, enable_locust=False):
        if not (cls._instance and singleton):
            cls._instance = super(OneViewPyClient, cls).__new__(cls)
        return cls._instance

    def __init__(self, singleton=True, enable_locust=False):
        self._enable_locust = enable_locust
        if enable_locust and not LOCUST_INSTALLED:
            log.error('Locust is not available on this platform')
            self._enable_locust = False

    def _get_token(self):
        try:
            credential = {
                'loginName': self._username,
                'password': self._password
            }
            response = requests.post(self._base_url + ('/auth/authentication/login' if not MODE == '18.03' else '/core/auth/token'),
                                     headers={'Content-Type': 'application/json'},
                                     data=json.dumps(credential))
            log.debug('Login response: {}'.format(response.json()))
            token1 = (response.json()['data'] if not MODE == '18.03' else json.loads(response.text))['token']
            if not token1:
                raise Exception('Authentication Failed')
            token = 'Bearer ' + str(token1)
            log.info('Authorized user: {}'.format(self._username))
            return token
        except Exception as e:
            log.error('Unable to authorize user: {}. Error: {}'.format(self._username, e))

    def get_username(self):
        return self._username

    def logout(self):
        response = self.get_response_data('/auth/authentication/logout')
        log.info('Logged out user: {}, token: {}'.format(self._username, self.token))
        self.token = None
        return response

    def get_entitlements(self):
        response = self.get_response_data('/auth/entitlement')
        return response

    def get_token_info(self):
        if self.token:
            response = self.get_response_data('/auth/authentication/token', params=dict(token=self.token.replace('Bearer ','')))
            return response

    def initialize(self, base_url, username, password, dev_mode=False, token_val=None):
        """
        :param base_url: eg - http://192.168.130.85:8000/oneview/rest
        :param username: eg - nxadmin
        :param password:
        :param dev_mode: if True, authentication (login) API will not be triggered. Some random value will be used as token.
            Backend needs to be working in dev mode else backend will reject subsequent API calls made with this dummy token
        :param token_val: if True, this value will be used as the token, real authentication (login) API will not be triggered.
            Backend needs to be working in dev mode else backend will reject subsequent API calls made with this dummy token
        :return:
        """
        self._base_url = base_url
        self._username = username
        self._password = password

        if token_val is not None:
            self.token = 'Bearer ' + token_val
            log.info('Bypassed authentication. user: {}, token: {}'.format(self._username, token_val))
        elif dev_mode:
            token_val = '***'
            self.token = 'Bearer ' + token_val
            log.info('Bypassed authentication. user: {}, token: {}'.format(self._username, token_val))
        else:
            self.token = self._get_token()

        # TODO remove callback if it is swagger specific
        def default_callback(response, oper):
            ov_status_code = json.loads(response.text)['statusCode']
            ov_status_message = json.loads(response.text)['statusMessage']

            log.debug("API:%s %s HTTPStatus:%s OVStatus:%s OVMessage:%s SwaggerOp:%s TimeReceived:%s" % (
                oper.http_method.upper(), oper.path_name, str(response.status_code), ov_status_code, ov_status_message,
                oper.operation_id,
                response.headers['Date']))

        self.callback = dict(response_callbacks=[default_callback])
        if self.token:
            return True
        else:
            return False

    def _get(self, url, params={}):
        response = requests.get(self._base_url + url, headers={'Authorization': self.token,
                                                               'Content-Type': 'application/json'}, params=params)
        return response

    def _post(self, url, body):
        response = requests.post(self._base_url + url, headers={'Authorization': self.token,
                                                                'Content-Type': 'application/json'}, data=body)
        return response

    def _delete(self, url):
        return requests.delete(self._base_url + url, headers={'Authorization': self.token,
                                                              'Content-Type': 'application/json'})

    def _put(self, url, body=None):
        return requests.put(self._base_url + url, headers={'Authorization': self.token,
                                                           'Content-Type': 'application/json'}, data=body)

    def _upload(self, url, data, filename, path):
        with open(path, 'rb') as f:
            log.debug("Upload %s %s" % (self._base_url + url, filename))
            return requests.post(self._base_url + url, data=data, files={'file': (filename, f)},
                                 headers={'Authorization': self.token})

    def process_response(self, response, start_time, name, json_response=True):
        # log.debug('Status: {}'.format(response.status_code))
        # log.debug('Elapsed ms: {}'.format(response.elapsed.microseconds / 1000))
        # log.debug('Method: {}'.format(response.request.method))
        # log.debug('URL: {}'.format(response.url))
        # log.debug('Headers: {}'.format(response.headers))
        # log.debug('Text: {}'.format(response.text))
        # log.debug('Content: {}'.format(response.content))
        # log.debug('Json: {}'.format(response.json()))
        # if 'error' in response.json(): log.debug('Error: {}'.format(response.json()['error']))
        # log.debug('Raw: {}'.format(response.raw))
        # log.debug('History: {}'.format(response.history))
        # log.debug('Raise for Status: {}'.format(response.raise_for_status()))
        # log.debug('Request body: {}'.format(json.dumps(response.request.body)))

        total_time = int((time.time() - start_time) * 1000)
        try:
            res_json = response.json()
        except:
            res_json = dict()
        success_message = '{name}: {method} {url} completed in {response_time}ms'.format(method=response.request.method,
                                                                                         url=response.url,
                                                                                         response_time=total_time,
                                                                                         name=name)
        if not response.status_code == requests.codes.ok:
            error = 'HTTP Status {}: {}'.format(response.status_code, res_json['error'] if 'error' in res_json else '')
            full_error = "{name}: {method} {url} failed in {response_time}ms with error : {error}".format(
                method=response.request.method,
                url=response.url, error=error, response_time=total_time, name=name)
            if response.request.body: full_error += '. Request body: {}'.format(response.request.body)
            if self._enable_locust:
                events.request_failure.fire(request_type=response.request.method, name=name, response_time=total_time,
                                            exception=full_error)
            log.error(full_error)
            raise Exception(full_error)
        elif json_response:
            if 'statusCode' in res_json and res_json['statusCode'] == 0:
                if self._enable_locust:
                    # Response length is in KB
                    events.request_success.fire(request_type=response.request.method, name=name,
                                                response_time=total_time,
                                                response_length=len(response.content) / 1000)
                log.debug(success_message)
                return res_json['data'] if 'data' in res_json else dict()
            else:
                error = 'HTTP Status {}, OV Status {}: {}'.format(response.status_code, res_json[
                    'statusCode'] if 'statusCode' in res_json else '', res_json[
                                                                      'statusMessage'] if 'statusMessage' in res_json else '')
                full_error = "{name}: {method} {url} failed in {response_time}ms with error : {error}".format(
                    method=response.request.method, url=response.url, error=error, response_time=total_time, name=name)
                if response.request.body: full_error += '. Request body: {}'.format(response.request.body)
                if self._enable_locust:
                    events.request_failure.fire(request_type=response.request.method, name=name,
                                                response_time=total_time,
                                                exception=full_error)
                log.error(full_error)
                raise Exception(full_error)
        else:
            if self._enable_locust:
                events.request_success.fire(request_type=response.request.method, name=name, response_time=total_time,
                                            response_length=0)
            log.debug(success_message)
            return response.content

    def get_response_data(self, url, params={}, name=None, json_response=True):
        start_time = time.time()
        if not name: name = url
        response = self._get(url, params=params)
        return self.process_response(response, start_time, name, json_response)

    def post_response_data(self, url, body, params={}, name=None, json_response=True):
        start_time = time.time()
        if not name: name = url
        params_str = ''
        if params:
            for (k, v) in params.items():
                params_str += k + '=' + v + '&'
            params_str = params_str[:-1]
            url = url + '?' + params_str
        response = self._post(url, body)
        return self.process_response(response, start_time, name, json_response)

    def put_response_data(self, url, body=None, params={}, name=None, json_response=True):
        start_time = time.time()
        if not name: name = url
        params_str = ''
        if params:
            for (k, v) in params.items():
                params_str += k + '=' + v + '&'
            params_str = params_str[:-1]
            url = url + '?' + params_str
        response = self._put(url, body)
        return self.process_response(response, start_time, name, json_response)

    def delete_response_data(self, url, name=None, json_response=True):
        start_time = time.time()
        if not name: name = url
        response = self._delete(url)
        return self.process_response(response, start_time, name, json_response)

    def upload_response_data(self, url, data, filename, path, name=None, json_response=True):
        start_time = time.time()
        if not name: name = url
        response = self._upload(url, data, filename, path)
        return self.process_response(response, start_time, name, json_response)

    def get_response_content(self, url, params={}, name=None):
        return self.get_response_data(url, params, name, json_response=False)

    def post_response_content(self, url, body, params={}, name=None):
        return self.post_response_data(url, body, params, name, json_response=False)

    def get_response_data_until(self, url, predicate, params={}, max_retry=10, sleep_time_sec=1):
        i = 0
        while True:
            result = self.get_response_data(url, params)
            i += 1
            if predicate(result): return result
            if i == max_retry: raise Exception(
                "GET API result is not satisfield the predicate {}".format(json.dumps(result)))
            time.sleep(sleep_time_sec)
