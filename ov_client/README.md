# README #
# Installation
Tested on Python 3.6. You can install as follows.
1. Install pip (version 9.0 and above), and wheel. For users using windows 10 and python 3.6, updating pip by 'easy_install --upgrade pip' may be needed.
2. Run 'python setup.py bdist_wheel' to build a wheel file in the dist directory.
3. Move to dist directory and tun 'python -m pip install {wheel file name}' to install the wheel file using pip.

#
# Development
* Use virtualenv and install by the above process for development, so that you can avoid to use unpackaged library unintentionally.
https://pypi.python.org/pypi/virtualenv

