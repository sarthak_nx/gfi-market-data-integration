import os
import logging
from datetime import datetime

BASE_SCRIPT_PATH = os.path.join("C:\\", "gfi-market-data-integration")
SCRIPT_PATH = os.path.join(BASE_SCRIPT_PATH, 'gateway', 'config')
CONFIG_PATH = os.path.join(BASE_SCRIPT_PATH, "configs")
FTP_PROPERTIES = "ftp_properties.json"
FILE_TYPE_PROPERTIES = "file_types.json"

FILE_STATUS_PATH = os.path.join(BASE_SCRIPT_PATH, "file_status")
if not os.path.exists(FILE_STATUS_PATH):
    os.makedirs(FILE_STATUS_PATH)

OUTPUT_PATH = os.path.join("C:\\", "Temp")
if not os.path.exists(OUTPUT_PATH):
    os.makedirs(OUTPUT_PATH)
TODAY = datetime.now().strftime('%Y-%m-%d')

log_path = os.path.join(BASE_SCRIPT_PATH, 'logs')

if not os.path.exists(log_path):
    os.makedirs(log_path)

log = logging.getLogger(os.path.join(log_path, 'gfi_market_data_apapter' + '-' + TODAY + '.log'))
logging.basicConfig(level=logging.DEBUG)
formatter = logging.Formatter('%(asctime)s %(levelname) -4s [%(pathname)s: %(funcName)s: #%(lineno)d]:    %(message)s')
handler = logging.FileHandler(os.path.join(log_path, 'ov_market_gateway' + '-' + TODAY + '.log'))
log.setLevel(logging.DEBUG)
handler.setFormatter(formatter)
log.addHandler(handler)